import logo from './logo.svg';
import { Fragment, useState, useEffect } from 'react';
import './App.css';
import BootstrapTable from 'react-bootstrap-table-next';
import request from 'superagent';
import Swal from 'sweetalert2'

const App = props => {

  const [datosTabla, setDatosTabla] = useState([])

  const columnasTabla = [{
    dataField: 'nombre',
    text: 'Nombre'
  }, {
    dataField: 'fase_maxima',
    text: 'Mejor transformación'
  }, {
    dataField: 'color_traje',
    text: 'Color traje'
  },
  {
    dataField: 'op',
    text: 'Operaciones'
  }];

  useEffect(() => {
    _obtenerSaiyajins()
  },[])

  const _obtenerSaiyajins = async () => {
    let respuesta_saiyajins = await request.get('http://localhost:3000/saiyajins')
      .set('Accept', 'application/json')
    console.log(respuesta_saiyajins.body);
    setDatosTabla(respuesta_saiyajins.body)
  }


  return (
    <Fragment>
      <div>
        <BootstrapTable keyField='id' data={datosTabla} columns={columnasTabla} />
      </div>
    </Fragment>

  );
}

export default App;
